package ru.inshakov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-json";
    }

    @NotNull
    @Override
    public String description() {
        return "load data from json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String filePath = serviceLocator.getPropertyService().getFileJsonPath("fasterXml");
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(filePath)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
