package ru.inshakov.tm.api.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Session;

public interface ISessionEndpoint {

    void closeSession(@NotNull Session session);

    @Nullable
    Session openSession(@NotNull String login, @NotNull String password);

}
