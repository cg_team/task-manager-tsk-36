package ru.inshakov.tm.api.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    List<E> findAll(final String userId);

    List<E> findAll(final String userId, final Comparator<E> comparator);

    void addAll(final String userId, final Collection<E> collection);

    E add(final String userId, final E entity);

    E findById(final String userId, final String id);

    void clear(final String userId);

    E removeById(final String userId, final String id);

    E remove(final String userId, final E entity);

    int getSize(final String userId);

}
