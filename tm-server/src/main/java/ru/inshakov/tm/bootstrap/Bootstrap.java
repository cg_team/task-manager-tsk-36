package ru.inshakov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.*;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.repository.*;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userService, taskService, projectService, sessionService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectService, projectTaskService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionService, userService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskService, projectTaskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userService, sessionService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void init() {
        initPID();
        initUsers();
        initEndpoints();
    }

    private void initUsers() {
        @NotNull final String adminId = userService.add("admin", "admin", "admin@a").getId();
        userService.findByLogin("admin").setRole(Role.ADMIN);
        @NotNull final String userId = userService.add("user", "user").getId();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

    @SneakyThrows
    private void initEndpoints(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        for (@NotNull final Class<? extends AbstractEndpoint> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

}
