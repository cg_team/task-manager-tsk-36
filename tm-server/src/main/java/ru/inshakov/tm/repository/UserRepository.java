package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(final String login) {
        if (login == null) return null;
        return entities.values().stream()
                .filter(o -> login.equals(o.getLogin()))
                .limit(1)
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(final String email) {
        if (email == null) return null;
        return entities.values().stream()
                .filter(o -> email.equals(o.getEmail()))
                .limit(1)
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User removeUserByLogin(final String login) {
        if (login == null) return null;
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user.getId());
        return user;
    }


}
