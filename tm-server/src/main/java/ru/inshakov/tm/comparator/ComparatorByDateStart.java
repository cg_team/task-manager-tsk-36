package ru.inshakov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.entity.IHasDateStart;
import ru.inshakov.tm.api.entity.IHasStartDate;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByDateStart implements Comparator<IHasStartDate> {

    @NotNull
    private static final ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    @NotNull
    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStartDate o1, @Nullable final IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
