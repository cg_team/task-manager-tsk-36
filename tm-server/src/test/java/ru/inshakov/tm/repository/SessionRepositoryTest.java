package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.inshakov.tm.model.Session;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private Session session;

    @Before
    public void before() {
        sessionRepository = new SessionRepository();
        @NotNull final Session session = new Session();
        session.setUserId("userId");
        this.session = sessionRepository.add(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("userId", session.getUserId());

        @NotNull final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionRepository.findById("34");
        Assert.assertNull(session);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Session session = sessionRepository.findById(null);
        Assert.assertNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId("34");
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId(null);
        Assert.assertNull(session);
    }

    @Test
    public void removeByUserId() {
        sessionRepository.removeByUserId(this.session.getUserId());
        Assert.assertEquals(0, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    public void removeByUserIdIncorrect() {
        sessionRepository.removeByUserId("34");
        Assert.assertEquals(1, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    public void removeByUserIdNull() {
        sessionRepository.removeByUserId(null);
        Assert.assertEquals(1, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    public void remove() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(sessionRepository.removeById(null));
    }

    @Test
    public void removeById() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(sessionRepository.removeById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Session session = sessionRepository.removeById("34");
        Assert.assertNull(session);
    }

}