package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.inshakov.tm.model.Project;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Project project;

    @Before
    public void before() {
        projectRepository = new ProjectRepository();
        project = projectRepository.add("testUser", new Project("Project"));
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll("testUser");
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = projectRepository.findById("testUser", this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Project project = projectRepository.findById("testUser", "34");
        Assert.assertNull(project);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Project project = projectRepository.findById("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void remove() {
        projectRepository.removeById(project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(projectRepository.removeById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Project project = projectRepository.findByName("testUser", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Project project = projectRepository.findByName("testUser", "34");
        Assert.assertNull(project);
    }

    @Test
    public void findByNameNull() {
        @NotNull final Project project = projectRepository.findByName("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findByIndex("testUser", 0);
        Assert.assertNotNull(project);
    }

    @Test
    public void removeById() {
        projectRepository.removeById("testUser", project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(projectRepository.removeById("testUser", null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Project project = projectRepository.removeById("testUser", "34");
        Assert.assertNull(project);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.removeById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = projectRepository.removeByIndex("testUser", 0);
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByName() {
        @NotNull final Project project = projectRepository.removeByName("testUser", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Project project = projectRepository.removeByName("testUser", "34");
        Assert.assertNull(project);
    }

    @Test
    public void removeByNameNull() {
        @NotNull final Project project = projectRepository.removeByName("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.removeByName("test", this.project.getName());
        Assert.assertNull(project);
    }

}
