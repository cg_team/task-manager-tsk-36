package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.inshakov.tm.exception.empty.EmptyEmailException;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyLoginException;
import ru.inshakov.tm.exception.empty.EmptyPasswordException;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.repository.UserRepository;

import java.util.List;

public class UserServiceTest {

    @Nullable
    private UserService userService;

    @Nullable
    private User user;

    @Before
    public void before() {
        userService = new UserService(new UserRepository(), new PropertyService());
        @NotNull final User user = new User();
        user.setLogin("User");
        user.setEmail("email");
        this.user = userService.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("User", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("email", user.getEmail());

        @NotNull final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        @NotNull final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final User user = userService.findById("34");
        Assert.assertNull(user);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final User user = userService.findById(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByLogin() {
        @NotNull final User user = userService.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLoginIncorrect() {
        @NotNull final User user = userService.findByLogin("34");
        Assert.assertNull(user);
    }

    @Test(expected = EmptyLoginException.class)
    public void findByLoginNull() {
        @NotNull final User user = userService.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByEmail() {
        @NotNull final User user = userService.findByEmail(this.user.getEmail());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByEmailIncorrect() {
        @NotNull final User user = userService.findByEmail("34");
        Assert.assertNull(user);
    }

    @Test(expected = EmptyEmailException.class)
    public void findByEmailNull() {
        @NotNull final User user = userService.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    public void remove() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        Assert.assertNull(userService.removeById(null));
    }

    @Test
    public void removeById() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        Assert.assertNull(userService.removeById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final User user = userService.removeById("34");
        Assert.assertNull(user);
    }

    @Test
    public void setPassword() {
        @NotNull final User user = userService.setPassword(this.user.getId(), "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = EmptyIdException.class)
    public void setPasswordUserIdNull() {
        @NotNull final User user = userService.setPassword(null, "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = UserNotFoundException.class)
    public void setPasswordUserIdIncorrect() {
        @NotNull final User user = userService.setPassword("null", "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = EmptyPasswordException.class)
    public void setPasswordNull() {
        @NotNull final User user = userService.setPassword("null", null);
        Assert.assertNotNull(user);
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(this.user.getLogin()));
    }

    @Test
    public void isLoginExistFalse() {
        Assert.assertFalse(userService.isLoginExist("test"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist(user.getEmail()));
    }

    @Test
    public void isEmailExistFalse() {
        Assert.assertFalse(userService.isEmailExist("e"));
    }

}