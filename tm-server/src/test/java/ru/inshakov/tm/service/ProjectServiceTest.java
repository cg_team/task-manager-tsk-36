package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private ProjectService projectService;

    @Nullable
    private Project project;

    @Before
    public void before() {
        projectService = new ProjectService(new ProjectRepository());
        project = projectService.add("testUser", new Project("Project"));
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll("testUser");
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = projectService.findById("testUser", this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Project project = projectService.findById("testUser", "34");
        Assert.assertNull(project);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Project project = projectService.findById("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        Assert.assertNull(projectService.removeById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Project project = projectService.findByName("testUser", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Project project = projectService.findByName("testUser", "34");
        Assert.assertNull(project);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Project project = projectService.findByName("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectService.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectService.findByIndex("testUser", 0);
        Assert.assertNotNull(project);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrect() {
        @NotNull final Project project = projectService.findByIndex("testUser", 34);
        Assert.assertNull(project);
    }

    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Project project = projectService.findByIndex("testUser", null);
        Assert.assertNull(project);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrectUser() {
        @NotNull final Project project = projectService.findByIndex("test", 0);
        Assert.assertNull(project);
    }

    @Test
    public void removeById() {
        projectService.removeById("testUser", project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        Assert.assertNull(projectService.removeById("testUser", null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Project project = projectService.removeById("testUser", "34");
        Assert.assertNull(project);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Project project = projectService.removeById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = projectService.removeByIndex("testUser", 0);
        Assert.assertNotNull(project);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrect() {
        projectService.removeByIndex("testUser", 34);
    }

    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        projectService.removeByIndex("testUser", null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrectUser() {
        projectService.removeByIndex("test", 0);
    }

    @Test
    public void removeByName() {
        @NotNull final Project project = projectService.removeByName("testUser", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Project project = projectService.removeByName("testUser", "34");
        Assert.assertNull(project);
    }

    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        projectService.removeByName("testUser", null);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Project project = projectService.removeByName("test", this.project.getName());
        Assert.assertNull(project);
    }

}
