package ru.inshakov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(@Nullable final String command) {
        super("Error! Command ``" + command + "`` not found...");
    }

}
