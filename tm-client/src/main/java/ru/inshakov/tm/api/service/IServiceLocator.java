package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

}
