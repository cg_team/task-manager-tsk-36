package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    String getApplicationVersion();

}
