package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.List;
import java.util.Optional;

public final class TasksShowByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks of project by project id";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        Optional.ofNullable(projectId).orElseThrow(ProjectNotFoundException::new);
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        tasks.forEach(System.out::println);
    }

}
