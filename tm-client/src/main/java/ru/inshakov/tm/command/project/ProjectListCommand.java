package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.user.AccessDeniedException;

import java.util.List;
import java.util.Optional;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show project list";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("[PROJECT LIST]");
        List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        projects.forEach(System.out::println);
    }

}
