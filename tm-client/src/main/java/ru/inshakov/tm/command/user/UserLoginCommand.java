package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "to enter in application";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final Session session = serviceLocator.getSessionEndpoint().openSession(login, password);
        serviceLocator.setSession(session);
    }

}

